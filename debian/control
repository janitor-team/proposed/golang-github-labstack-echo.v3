Source: golang-github-labstack-echo.v3
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Nobuhiro Iwamatsu <iwamatsu@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-dgrijalva-jwt-go-dev (>= 3.0~),
               golang-github-labstack-gommon-dev,
               golang-github-stretchr-testify-dev,
               golang-github-valyala-fasttemplate-dev,
               golang-golang-x-crypto-dev,
               golang-gopkg-tylerb-graceful.v1-dev,
               golang-github-valyala-fasthttp-dev
Standards-Version: 4.1.1
Homepage: https://github.com/labstack/echo
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-labstack-echo.v3.git
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-labstack-echo.v3
XS-Go-Import-Path: github.com/labstack/echo.v3
Testsuite: autopkgtest-pkg-go

Package: golang-github-labstack-echo.v3-dev
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         golang-github-dgrijalva-jwt-go-dev (>= 3.0~),
         golang-github-labstack-gommon-dev,
         golang-github-stretchr-testify-dev,
         golang-github-valyala-fasttemplate-dev,
         golang-golang-x-crypto-dev,
         golang-gopkg-tylerb-graceful.v1-dev,
         golang-github-valyala-fasthttp-dev
Description: Echo is a fast and unfancy HTTP server framework for Golang
 Echo is a minimalist Go web framework that provides the following functions:
  - Optimized HTTP router which smartly prioritize routes
  - Build robust and scalable RESTful APIs
  - Group APIs
  - Extensible middleware framework
  - Define middleware at root, group or route level
  - Data binding for JSON, XML and form payload
  - Handy functions to send variety of HTTP responses
  - Centralized HTTP error handling
  - Template rendering with any template engine
  - Define your format for the logger
  - Highly customizable
  - Automatic TLS via Let’s Encrypt
  - HTTP/2 support
